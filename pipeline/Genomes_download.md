# Get bacteria genomes and annotations from NCBI

We get bacteria genomes and annotations from NCBI, with NCBI's command-line tool `datasets`.

Here are some shell examples from official help:


```
datasets download genome accession GCF_000001405.40 --chromosomes X,Y --exclude-gff3 --exclude-rna 
datasets download genome taxon "bos taurus" --dehydrated
datasets download genome taxon human --assembly-level chromosome,complete_genome --dehydrated
datasets download genome taxon mouse --search C57BL/6J --search "Broad Institute" --dehydrated
```

This command can download genome based on accession IDs (even list of ID, txt format) or taxons or just searching the key words.

Normally these flags are usully considered in my project with `datasets download genome taxon bacteria `:

```
--annotated  
--assembly-level complete_genome  
--assembly-source refseq 
--include-gbff 
--include-gtf 
--reference  
--released-before MM/DD/YYYY 
--released-since MM/DD/YYYY 
--dehydrated
```

- **--dehydrated** can make you download large genomes completely without missings(but remember to [rehydrated](https://www.ncbi.nlm.nih.gov/datasets/docs/v2/how-tos/genomes/large-download/) and unzip).
