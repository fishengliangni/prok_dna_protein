For each prokaryote, homoloy were found between the genome and a set of protein sequences from Swissprot, Uniprot(Transposon-related) and RepeatMasker.

In this research, we mainly used LAST for analysis.

### 1. Use last-train to obtain more accurate results.

We firstly used `last-train` to **learn** the insertion, deletion, and substitutions between *M.Leprae* pseudogenes and *M.tuberculosis* proteins in the following script:

```
lastdb -q -c mtuber_db Mycobacterium_tuberculosis_protein.faa
```

```
awk '$3 == "pseudogene"' $d/Mycobacterium_leprae_genomic.gff | seg-import gff |
   seg-seq - Mycobacterium_leprae_dna.fna |
   last-train --codon -X1 mtuber_db > ~/proj/prok-fossil/mat/prokaryotes_pseudoge.train
```

The above gets *M.Leprae* pseudogenes DNA sequences and trains them against *M.tuberculosis* proteins.

The reason for choosing *M.Leprae* pseudogenes and *M.tuberculosis* proteins is that *M.Leprae* has a lot of pseudogene and *M.tuberculosis* proteins always have their non-seudoge homologs. 

We think this `train file` can be reused to help us find DNA-Protein homolog in other prokaryote genomes, and also capture the DNA-Protein homolog as many as possible.

Of course, users can select the DNA sequences or amino acids whichever they like or they think it is meaningful to study their insertion, deletion and substitutions.


### 2. `lastal` will find the DNA-Protein homologs


```
fasta-nr hostProteins reference_protein.fasta | lastdb -q -c pDB
lastal -D1e9 -K0 -m500 -p prokaryotes_pseudoge.train pDB prot_genome.fa > aln.maf
```

Option `-D1e9` sets the significance threshold to one false hit per 109 bp 
`-K0` omits hits that overlap stronger hits in the genome
`-m500` makes alignment much slower and more sensitive.

### 3. `Protein-fossils` suites help to extract DNA-Protein homologs in prokaryotes intergenic regions

Details of `Protein-fossils` suites are described in [here](https://gitlab.com/mcfrith/protein-fossils).

`maf-pseudogene` will help us count frameshifts and premature stop codons in DNA-to-protein
alignments

`protein-fossils-te` will help to extract the basic information (psl format, coordinates) of alignments.


