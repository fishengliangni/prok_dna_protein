1. ```NZ_CP082833.1	5341875	4119938	4120127    sp|P0DUW2|YNHH_ECOLI    Encoded upstream of the pykF promoter```
2. ```NZ_CP013199.1 1599709	1231655	12318055    sp|Q88424|ORF13_SPV1C   transmembrane protein of Plectrovirus spv1```
3. ```NZ_CP083405.1	3271694	1851249	18523141    sp|O33194|G3PP_MYCTU    G3PP, final enzyme involved in the recycling/catabolism of glycerophospholipid polar heads```
4. ```NC_000913.3	4641652	2470763	24711051    sp|P77326|TFAS_ECOLI    Putative protein TfaS(properly pseudogenes)```

