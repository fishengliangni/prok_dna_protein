# DNA-Protein homologs in Prokaryote genomes intergenic regions

This is an ongoing research project about **protein fossils** in prokaryotes. 

Protein fossils are **segments** of **non-protein-coding DNA descended from protein-coding DNA** (**Frameshift!**). 

We believe protein fossils can persist for **long evolutionary time periods**. 

The analysis of them could provide a novel insight into prokaryotic evolution history, maybe as a temporary record of phenotypic traits that have been ignored.



The `tutorials` directory contains all the code necessary to find **protein fossils candidates** ( homologs of DNA-Protein, part of pseudogenes？)

The tutorials directory consists of These notes:

1. `Genomes_download.md` describe how to get bacteria genomes and annotations from NCBI, with `datasets`
2. `Homologs_find.md` exlain how to extract the protein Fossils candidates from genomes and proteins data.
3. `Homologs_arrangement.md` was used to extract the homologs information from Swissprot.
4. `Interesting_homologs.md` record the interesting DNA-Protain homologs in this project.
